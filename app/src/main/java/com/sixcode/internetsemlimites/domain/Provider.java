package com.sixcode.internetsemlimites.domain;


import java.io.Serializable;
import java.util.List;

public class Provider implements Serializable {
    private String name;
    private String moderation;
    private String other;
    private String source;
    private String category;
    private String created_at;
    private String status;
    private String url;
    private List<String> coverage;

    public Provider() {
    }

    public Provider(String name, String moderation, String other, String source, String category, String created_at, String status, String url, List<String> coverage) {
        this.name = name;
        this.moderation = moderation;
        this.other = other;
        this.source = source;
        this.category = category;
        this.created_at = created_at;
        this.status = status;
        this.url = url;
        this.coverage = coverage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModeration() {
        return moderation;
    }

    public void setModeration(String moderation) {
        this.moderation = moderation;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<String> getCoverage() {
        return coverage;
    }

    public void setCoverage(List<String> coverage) {
        this.coverage = coverage;
    }
}
