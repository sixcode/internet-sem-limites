package com.sixcode.internetsemlimites.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.gson.Gson;
import com.sixcode.internetsemlimites.R;
import com.sixcode.internetsemlimites.adapters.ViewPagerAdapter;
import com.sixcode.internetsemlimites.domain.Estado;
import com.sixcode.internetsemlimites.domain.Provider;
import com.sixcode.internetsemlimites.fragments.FragmentComLimite;
import com.sixcode.internetsemlimites.fragments.FragmentSemLimite;
import com.sixcode.internetsemlimites.services.MainService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {

    private Gson gson = new Gson();
    private List<Provider> hallOfShame = new ArrayList<>();
    private List<Provider> hallOfFame = new ArrayList<>();
    private List<Estado> listEstados = new ArrayList<>();
    private List<String> listEstadosFame = new ArrayList<>();
    private List<String> listEstadosShame = new ArrayList<>();
    private HashMap<String, List<Provider>> listItensGrupoShame = new HashMap<>();
    private HashMap<String, List<Provider>> listItensGrupoFame =  new HashMap<>();

    public SharedPreferences appPreferences;
    public boolean isAppInstalled = false;

    private Toolbar toolbar;

    private ViewPager viewPager;
    private TabLayout tabLayout;
    private ViewPagerAdapter viewPagerAdapter;

    private FragmentComLimite fragmentComLimite;
    private FragmentSemLimite fragmentSemLimite;

    private  AdView mAdView;

    private AlertDialog alerta;

    int cont = 0;

    InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        appPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        //createOrUpdateShortcut();
        
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        viewPager = (ViewPager) findViewById(R.id.pagerFormCompras);

        carregarEstados();

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-5829449767839495/3629463969");

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();

            }
        });

        requestNewInterstitial();

        final ProgressDialog dialog = ProgressDialog.show(MainActivity.this, "", "Carregando...\nAguarde por favor...", true);
        dialog.show();

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(30 * 1000, TimeUnit.MILLISECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://internetsemlimites.herokuapp.com")
                .client(okHttpClient)
                .build();

        MainService service = retrofit.create(MainService.class);

        final Call<ResponseBody> callResponse = service.resposta();

        callResponse.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                JSONObject jsonResponse = null;
                try {
                    jsonResponse = new JSONObject(response.body().string());

                    if (jsonResponse.has("hall-of-shame")) {
                        JSONArray jsonArrayShame = jsonResponse.getJSONArray("hall-of-shame");

                        for (int i = 0; i < jsonArrayShame.length(); i++) {
                            JSONObject objectShame = jsonArrayShame.getJSONObject(i);

                            Provider provider = gson.fromJson(objectShame.toString(), Provider.class);

                            hallOfShame.add(provider);
                        }

                    }

                    if (jsonResponse.has("hall-of-fame")) {
                        JSONArray jsonArrayFame = jsonResponse.getJSONArray("hall-of-fame");

                        for (int i = 0; i < jsonArrayFame.length(); i++) {
                            JSONObject objectFame = jsonArrayFame.getJSONObject(i);

                            Provider provider = gson.fromJson(objectFame.toString(), Provider.class);

                            hallOfFame.add(provider);
                        }

                    }

                    gerarRelacionamento();

                    dialog.dismiss();

                    setupViewPager(viewPager);

                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(final Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Erro ao carregar");
                builder.setMessage("Não foi possível carregar a lista das operadoras.\nPode ser que a sua operadora já esteja limitando a internet!");
                builder.setPositiveButton("Tentar Novamente", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alerta.dismiss();
                        Intent intent = getIntent();
                        finish();
                        startActivity(intent);
                    }
                });
                alerta = builder.create();
                alerta.show();
            }
        });

    }

    private void createOrUpdateShortcut() {
        isAppInstalled = appPreferences.getBoolean("isAppInstalled",false);
        if(isAppInstalled == false) {
            Intent HomeScreenShortCut= new Intent(getApplicationContext(), MainActivity.class);
            HomeScreenShortCut.setAction(Intent.ACTION_MAIN);
            HomeScreenShortCut.putExtra("duplicate", false);
            //shortcutIntent is added with addIntent
            Intent addIntent = new Intent();
            addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, HomeScreenShortCut);
            addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, "Internet Sem Limites");
            addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
                    Intent.ShortcutIconResource.fromContext(getApplicationContext(),
                            R.mipmap.ic_launcher));

            addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");

            getApplicationContext().sendBroadcast(addIntent);

            SharedPreferences.Editor editor = appPreferences.edit();
            editor.putBoolean("isAppInstalled", true);
            editor.commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_about) {
            if (mInterstitialAd.isLoaded() && (cont == 0 || cont == 2)) {
                cont = 1;
                mInterstitialAd.show();
                Intent telaSobre = new Intent(MainActivity.this, AboutActivity.class);
                startActivity(telaSobre);
            } else {
                cont = cont + 1;
                Intent telaSobre = new Intent(MainActivity.this, AboutActivity.class);
                startActivity(telaSobre);
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void carregarEstados() {
        listEstados.add(new Estado("Acre", "AC"));
        listEstados.add(new Estado("Alagoas", "AL"));
        listEstados.add(new Estado("Amapá", "AP"));
        listEstados.add(new Estado("Amazonas", "AM"));
        listEstados.add(new Estado("Bahia", "BA"));
        listEstados.add(new Estado("Ceará", "CE"));
        listEstados.add(new Estado("Distrito Federal", "DF"));
        listEstados.add(new Estado("Espírito Santo", "ES"));
        listEstados.add(new Estado("Goiás", "GO"));
        listEstados.add(new Estado("Maranhão", "MA"));
        listEstados.add(new Estado("Mato Grosso", "MT"));
        listEstados.add(new Estado("Mato Grosso do Sul", "MS"));
        listEstados.add(new Estado("Minas Gerais", "MG"));
        listEstados.add(new Estado("Pará", "PA"));
        listEstados.add(new Estado("Paraíba", "PB"));
        listEstados.add(new Estado("Paraná", "PR"));
        listEstados.add(new Estado("Pernambuco", "PE"));
        listEstados.add(new Estado("Piauí", "PI"));
        listEstados.add(new Estado("Rio de Janeiro", "RJ"));
        listEstados.add(new Estado("Rio Grande do Norte", "RN"));
        listEstados.add(new Estado("Rio Grande do Sul", "RS"));
        listEstados.add(new Estado("Rondônia", "RO"));
        listEstados.add(new Estado("Roraima", "RR"));
        listEstados.add(new Estado("Santa Catarina", "SC"));
        listEstados.add(new Estado("São Paulo", "SP"));
        listEstados.add(new Estado("Sergipe", "SE"));
        listEstados.add(new Estado("Tocantins", "TO"));
    }

    public void gerarRelacionamento() {

        for (Estado uf : listEstados) {
            List<Provider> provedoresEstado = new ArrayList<>();

            for (Provider fame : hallOfFame) {
                if(fame.getCoverage().contains(uf.getSigla())) {
                    provedoresEstado.add(fame);

                    if (!listEstadosFame.contains(uf.getNome()))
                        listEstadosFame.add(uf.getNome());
                }
            }

            if (provedoresEstado != null && provedoresEstado.size() > 0)
                listItensGrupoFame.put(uf.getNome(), provedoresEstado);

            provedoresEstado = new ArrayList<>();

            for (Provider shame : hallOfShame) {
                if (shame.getCoverage().contains(uf.getSigla())) {
                    provedoresEstado.add(shame);

                    if (!listEstadosShame.contains(uf.getNome()))
                        listEstadosShame.add(uf.getNome());
                }
            }

            if (provedoresEstado != null && provedoresEstado.size() > 0)
                listItensGrupoShame.put(uf.getNome(), provedoresEstado);
        }

    }

    private void setupViewPager(ViewPager pViewPager) {
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        fragmentSemLimite = new FragmentSemLimite();
        Bundle args1 = new Bundle();
        args1.putStringArrayList("listEstadosFame", (ArrayList<String>) listEstadosFame);
        args1.putSerializable("listItensGrupoFame", listItensGrupoFame);
        fragmentSemLimite.setArguments(args1);
        viewPagerAdapter.addFragment(fragmentSemLimite, "Sem Limites");

        fragmentComLimite = new FragmentComLimite();
        Bundle args = new Bundle();
        args.putStringArrayList("listEstadosShame", (ArrayList<String>) listEstadosShame);
        args.putSerializable("listItensGrupoShame", listItensGrupoShame);
        fragmentComLimite.setArguments(args);
        viewPagerAdapter.addFragment(fragmentComLimite, "Com Limites");

        viewPager.setAdapter(viewPagerAdapter);

        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(pViewPager);
        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.white));

        setupTabIcons();
    }

    private void setupTabIcons() {
        TextView newTab = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        newTab.setText("Sem Limites");
        newTab.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_sem_limites, 0, 0, 0);
        tabLayout.getTabAt(0).setCustomView(newTab);

        TextView newTab2 = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        newTab2.setText("Com Limites");
        newTab2.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_com_limite, 0, 0, 0);
        tabLayout.getTabAt(1).setCustomView(newTab2);
    }

    @Override
    protected void onPause() {
        mAdView.pause();
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mAdView.resume();
    }

    @Override
    protected void onDestroy() {
        mAdView.destroy();
        super.onDestroy();
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("SEE_YOUR_LOGCAT_TO_GET_YOUR_DEVICE_ID")
                .build();

        mInterstitialAd.loadAd(adRequest);
    }
}