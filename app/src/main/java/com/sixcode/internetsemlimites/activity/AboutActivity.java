package com.sixcode.internetsemlimites.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.ads.InterstitialAd;
import com.sixcode.internetsemlimites.R;

public class AboutActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView tvemail, tvSobreSix;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        tvemail = (TextView) findViewById(R.id.tvEmail);
        tvSobreSix = (TextView) findViewById(R.id.tvSobreSix);
        tvSobreSix.setText(Html.fromHtml(getString(R.string.aboutSix)));

        tvemail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + "thesixcode@gmail.com" +
                        "?subject=" + Uri.encode("contato Internet Sem Limites")));
                startActivity(Intent.createChooser(emailIntent, "Enviar e-mail utilizando:"));
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}