package com.sixcode.internetsemlimites.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.sixcode.internetsemlimites.R;
import com.sixcode.internetsemlimites.adapters.ProvidersAdapter;
import com.sixcode.internetsemlimites.domain.Provider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FragmentSemLimite extends Fragment{

    private List<String> listEstadosFame = new ArrayList<>();
    private HashMap<String, List<Provider>> listItensGrupoFame = new HashMap<>();
    private ExpandableListView elvProvedores;

    public FragmentSemLimite() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listEstadosFame = getArguments().getStringArrayList("listEstadosFame");
        listItensGrupoFame = (HashMap<String, List<Provider>>) getArguments().getSerializable("listItensGrupoFame");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.fragment_semlimite, container, false);
        elvProvedores = (ExpandableListView) v.findViewById(R.id.elvProvedores);

        ProvidersAdapter adapter = new ProvidersAdapter(listEstadosFame, listItensGrupoFame, getContext());
        elvProvedores.setAdapter(adapter);

        return v;
    }
}
