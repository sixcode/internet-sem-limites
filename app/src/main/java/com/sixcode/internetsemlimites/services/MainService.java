package com.sixcode.internetsemlimites.services;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface MainService {

    @Headers({
            "Content-Type: application/json;charset=utf-8",
            "Accept: application/json"
    })

    @GET("/")
    Call<ResponseBody> resposta();

}
