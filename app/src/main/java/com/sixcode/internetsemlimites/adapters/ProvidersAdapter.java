package com.sixcode.internetsemlimites.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.sixcode.internetsemlimites.R;
import com.sixcode.internetsemlimites.domain.Provider;

import java.util.HashMap;
import java.util.List;

public class ProvidersAdapter extends BaseExpandableListAdapter {

    private List<String> listEstados;
    private HashMap<String, List<Provider>> listProvedores;
    private Context context;

    public ProvidersAdapter(List<String> listEstados, HashMap<String, List<Provider>> listProvedores, Context context) {
        this.listEstados = listEstados;
        this.listProvedores = listProvedores;
        this.context = context;
    }

    @Override
    public int getGroupCount() {
        return listEstados.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return listProvedores.get(getGroup(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return listEstados.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return listProvedores.get(getGroup(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.row_estados, null);
        }

        TextView tvGrupo = (TextView) convertView.findViewById(R.id.tvGrupo);
        TextView tvQtde = (TextView) convertView.findViewById(R.id.tvQtde);

        tvGrupo.setText((String) getGroup(groupPosition));
        tvQtde.setText(String.valueOf(getChildrenCount(groupPosition)));

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.row_provedores, null);
        }

        TextView tvProvedor = (TextView) convertView.findViewById(R.id.tvProvedor);
        TextView tvSite = (TextView) convertView.findViewById(R.id.tvSite);
        TextView tvFonte = (TextView) convertView.findViewById(R.id.tvFonte);

        final Provider provedor = (Provider) getChild(groupPosition, childPosition);
        tvProvedor.setText("Provedor: " + provedor.getName());
        tvSite.setText("Site: " + provedor.getUrl());
        tvFonte.setText("Fonte: " + provedor.getSource());

        tvSite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(provedor.getUrl()));
                context.startActivity(i);
            }
        });

        tvFonte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(provedor.getSource()));
                context.startActivity(i);
            }
        });

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
